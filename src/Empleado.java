public class Empleado {

    private String nombre;
    private String cedula;
    private String puesto;

    //Constructor for defecto

    public Empleado(){
        this.nombre = "-";
        this.cedula = "-";
        this.puesto = "-";
    }


    public Empleado(String nombre, String cedula,String puesto) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "nombre='" + nombre + '\'' +
                ", cedula='" + cedula + '\'' +
                ", puesto='" + puesto + '\'' +
                '}';
    }
}
